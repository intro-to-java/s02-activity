package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        int year = sc.nextInt();

        if(year % 4 == 0) {
            if(year % 100 == 0) {
                if(year % 400 == 0) {
                    System.out.println(year + " is a leap year");
                }
                System.out.println(year + " is NOT a leap year");
            }
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is NOT a leap year");
        }

//        if(year % 4 == 0 || year % 400 == 0) {
//            System.out.println(year + " is a leap year");
//        } else {
//            System.out.println(year + " is NOT a leap year");
//        }

    }
}
